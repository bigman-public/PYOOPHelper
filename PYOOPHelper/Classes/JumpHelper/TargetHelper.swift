//
//  TargetHelper.swift
//  Pods
//
//  Created by yunhe.lin on 17/2/16.
//
//

import UIKit

public class TargetHelper: NSObject {
    
    var targets: [String : AnyClass] = [:]
    
    
    public static let instance: TargetHelper = {
        return TargetHelper()
    }()
}

public protocol TargetHelperProtocol {
    
    @discardableResult
    func register(target: String, _ forViewController: AnyClass) -> Bool
    
    @discardableResult
    func viewController(_ target: String) -> UIViewController?
    
    @discardableResult
    func register(targets: [String : AnyClass])
    
}

extension TargetHelper: TargetHelperProtocol {
    
    @discardableResult
    public func register(target: String, _ forViewController: AnyClass) -> Bool {
        
        guard let vc = forViewController as? UIViewController.Type else {
            return false
        }
        targets[target] = forViewController
        return true
    }
    
    public func viewController(_ target: String) -> UIViewController? {
        
        guard let cls = targets[target] as? UIViewController.Type else {
            return nil
        }
        let targetVC = cls.init()        
        return targetVC
    }
    
    public func register(targets: [String : AnyClass]) {
        targets.keys.forEach { (key) in
            register(target: key, targets[key]!)
        }
    }
}
