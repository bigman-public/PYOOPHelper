//
//  TestViewController.swift
//  Pods
//
//  Created by yunhe.lin on 17/2/16.
//
//

import UIKit

public class TestViewController: UIViewController, ReleationProtocol {
    
    public var releationCallBack: ReleationCallBack?
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension TestViewController: TargetValidParam {
    
    public func valid(_ params: [String : AnyObject]?) -> Bool {
        print(params)
        return true
    }

}
