//
//  NextPageState.swift
//  Pods
//
//  Created by yunhe.lin on 17/2/15.
//
//

import Foundation

public struct NextPageState<T> {
    
    public var hasNext: Bool
    public var isLoading: Bool
    public var lastId: T?
    
    public init() {
        hasNext = true
        isLoading = false
        lastId = nil
    }
    
    mutating func reset() {
        hasNext = true
        isLoading = false
        lastId = nil
    }
    
    mutating func update(hasNext: Bool, isLoading: Bool, lastId: T?) {
        self.hasNext = hasNext
        self.isLoading = isLoading
        self.lastId = lastId
    }
    
}
