//
//  JumpHelper.swift
//  Pods
//
//  Created by yunhe.lin on 17/2/16.
//
//

import UIKit

public typealias ReleationCallBack = (([String : AnyObject]) -> Bool)

public protocol TargetValidParam {
    
    func valid(_ params: [String : AnyObject]?) -> Bool
    
}

public protocol ReleationProtocol {
    
    var releationCallBack : ReleationCallBack? {get set}
    
}

public class JumpHelper: NSObject {
    
    
    fileprivate var navigators: [UINavigationController] = []
    fileprivate var index: Int = -1
    
    public static let instance: JumpHelper = {
        return JumpHelper()
    }()
    
    override fileprivate init() {
        super.init()
    }
    
    public func initNavigators(_ navigationControllers: [UINavigationController]) -> Void {
        DispatchQueue.once(token: "jumpHelper.init.navigators") { 
            navigators += navigationControllers
            if navigationControllers.count > 0 {
                index = 0;
            }
        }
    }
    
    public func didSelect(index: Int = 0) {
        guard index > navigators.count else {
            return
        }
        self.index = index
    }
    
}

extension JumpHelper {
    
    // MARK: - 缺少拦截机制
    public func pushViewController(_ url: String?, 
                            _ params: [String : AnyObject]? = nil,
                            _ animated: Bool = true,
                            _ releationCallBack: ReleationCallBack? = nil) {
        
        guard (url != nil) else {
            return
        }
        
        guard let vc = TargetHelper.instance.viewController(url!) else {
            return
        }
        
        if var releationVC = vc as? ReleationProtocol  {
            releationVC.releationCallBack = releationCallBack
        }
        
        guard let paramVC = vc as? TargetValidParam else {
            cureentNavigationController().pushViewController(vc, animated: animated)
            return
        }
        
        let isPushed = paramVC.valid(params)
        
        if isPushed {
            cureentNavigationController().pushViewController(vc, animated: animated)
        }
    }
    
    public func cureentNavigationController() -> UINavigationController {
        return self.navigators[index]
    }
    
}
