
Pod::Spec.new do |s|
  def self.smart_version
    tag = `git describe --abbrev=0 --tags 2>/dev/null`.strip
    if $?.success? then tag else "0.0.1" end
  end
  
  s.name             = 'PYOOPHelper'
  s.version          = smart_version
  s.summary          = ' description of PYOOPHelper.'
  s.description      = <<-DESC
PYOOPHelper
                       DESC

  s.homepage         = 'https://gitlab.com/bigman-public/PYOOPHelper/PYOOPHelper'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'yunhe.lin' => 'bigmanhxx@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/bigman-public/PYOOPHelper.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'PYOOPHelper/Classes/**/*.*'
  
  s.resource_bundles = {
    'PYOOPHelper' => ['PYOOPHelper/Recourse/**/*.*', 'PYOOPHelper/Recourse/Images/Media.xcassets']
  }

  s.public_header_files = 'Pod/Classes/**/*.h'

end
