//
//  URLHandler.swift
//  Pods
//
//  Created by yunhe.lin on 17/2/15.
//
//

import UIKit

private let defaultHandlers: [URLHandlerable] = [URLHandlerApp()]
private var urlHandlers: [URLHandlerable] = [URLHandlerable]()

public class URLHandler: NSObject {
    
    /// 注册handlers
    ///
    /// - Parameter handlers: [URLHandlerable]
    public class func register(handlers: [URLHandlerable]?) {
        DispatchQueue.once(token: "oophelper.once.register.handler") { 
            if (handlers != nil) {
                urlHandlers += handlers!
            }
            // 添加默认handlers
            urlHandlers += defaultHandlers
        }
    }
    
    
    /// 打开方法
    ///
    /// - Parameter targetURL: 目标URL
    /// - Returns: 是否成功
    @discardableResult
    public class func open(targetURL: URL) -> Bool {
        var isOpen = false
        for handler in urlHandlers {
            isOpen = handler.open(targetURL)
            if isOpen { break }
        }
        return isOpen
    }
    
    
    /// 打开方法
    ///
    /// - Parameter targetUrl: 目标url
    /// - Returns: 是否成功
    @discardableResult
    public class func open(targetUrl: String?) -> Bool {
        guard targetUrl != nil else {
            return false
        }
        guard let u = URL.init(string: targetUrl!) else {
            return false
        }
        return open(targetURL: u)
    }
    
}

