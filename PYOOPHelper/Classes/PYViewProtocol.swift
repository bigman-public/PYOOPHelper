//
//  PYViewProtocol.swift
//  Pods
//
//  Created by yunhe.lin on 17/2/10.
//
//

import Foundation

private let kPYImageViewLoadingId = "py_imageLoading"
private let kPYImageViewErrorId = "py_imageError"
private let kPYErrorImageViewId = "py_errorImageView"

typealias py_imageErrorHandler = ()->Void

public protocol PYViewProtocol {}
public protocol PYImageViewProtocol {}
public protocol PYLabelProtocol {}

//MARK: - label py_method
public extension PYLabelProtocol where Self: PYLabel {
    
    @discardableResult
    public func py_font(_ font: UIFont) -> PYLabel {
        self.font = font
        return self
    }
    
    @discardableResult
    public func py_text(_ str: String) -> PYLabel {
        self.text = str
        return self
    }
    
    @discardableResult
    public func py_textAlignment(_ textAlignment: NSTextAlignment) -> PYLabel {
        self.textAlignment = textAlignment
        return self
    }
    
    @discardableResult
    public func py_textColor(_ textColor: UIColor) -> PYLabel {
        self.textColor = textColor
        return self
    }
    
    @discardableResult
    public func py_numberOfLines(_ lines: Int) -> PYLabel {
        self.numberOfLines = lines
        return self
    }
}

// MARK: - imageView py_method
public extension PYImageViewProtocol where Self: UIImageView {
    
    // MARK: - 加载动画部分
    private func loadingView() -> UIActivityIndicatorView {
        let indicatorView = objc_getAssociatedObject(self, kPYImageViewLoadingId)
        if ((indicatorView as? UIActivityIndicatorView) != nil) {
            return indicatorView as! UIActivityIndicatorView
        }
        let v = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        v.hidesWhenStopped = true
        objc_setAssociatedObject(self, kPYImageViewLoadingId, v, .OBJC_ASSOCIATION_RETAIN)
        return v
    }
    
    
    /// 加载imageView loading动画
    public func py_loading() {
        
        self.py_resetError()
        
        let v = self.loadingView()
        if v.superview == nil {
            self.addSubview(v)
            v.center = CGPoint.init(x: self.frame.size.width/2.0, y: self.frame.size.height/2.0)
        }
        v.startAnimating()
    }
    
    
    /// 去除加载动画
    public func py_dismissLoading() {
        let v = self.loadingView()
        v.stopAnimating()
    }
    
    // MARK: -加载失败部分
    
    /// 失败 点击回调方法 暂不开放
    ///
    /// - Parameter handler: 错误view 点击回调
    private func py_setError(handler: py_imageErrorHandler) {
        guard handler != nil else {
            return;
        }
        objc_setAssociatedObject(self, kPYImageViewErrorId, handler, .OBJC_ASSOCIATION_COPY)
    }
    
    private func errorDownloadHandler() -> py_imageErrorHandler? {
        let errorHandler = objc_getAssociatedObject(self, kPYImageViewErrorId) as? py_imageErrorHandler
        return errorHandler
    }
    
    
    /// 加载失败方法
    public func py_failLoading() {
        self.py_dismissLoading()
        let v = self.errorView()
        if v.superview == nil {
            self.addSubview(v)
            v.center = CGPoint.init(x: self.frame.size.width/2.0, y: self.frame.size.height/2.0)
        }
        self.bringSubview(toFront: v)
        v.isHidden = false
    }
    
    
    /// 重置加载失败方法
    public func py_resetError() {
        self.errorView().isHidden = true
    }
    
    /// 加载失败 错误view
    ///
    /// - Returns: imageView
    private func errorView() -> UIImageView {
        let errorImageView = objc_getAssociatedObject(self, kPYErrorImageViewId) as? UIImageView
        if errorImageView != nil {
            return errorImageView!
        }
        let v = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 20, height: 20))
        v.backgroundColor = UIColor.black
        objc_setAssociatedObject(self, kPYErrorImageViewId, v, .OBJC_ASSOCIATION_RETAIN)
        return v
    }
}

// MARK: - view py_method
public extension PYViewProtocol where Self: UIView {
    
    
    /// 淡出操作
    ///
    /// - Parameter animated: 是否存在动画
    public func py_pop(animated: Bool) {
        guard animated else {
            self.alpha = 0
            return
        }
        UIView.animate(withDuration: 1) {  
            self.alpha = 0
        }
    }
    
    /// 抖动动画
    ///
    /// - Parameter repeatCount: 晃动次数
    public func py_shake(repeatCount: Float) {
        
        let shakeAnimation = CABasicAnimation.init(keyPath: "transform.rotation.z")
        shakeAnimation.fromValue = 0.1
        shakeAnimation.toValue = -0.1
        shakeAnimation.autoreverses = true
        shakeAnimation.repeatCount = (repeatCount > 0 ? repeatCount : 1)
        shakeAnimation.duration = 0.1
        self.layer.add(shakeAnimation, forKey: "shakeAnimateID")
        
    }
    
    
    /// 左右抖动动画
    ///
    /// - Parameter repeatCount: 重复次数
    public func py_spring(repeatCount: Float) {
        let position = self.layer.position
        let pointLeft = CGPoint.init(x: CGFloat(position.x - 10), y: CGFloat(position.y))
        let pointRight = CGPoint.init(x: CGFloat(position.x + 10), y: CGFloat(position.y))
        let shakeAnimation = CABasicAnimation.init(keyPath: "position")
        shakeAnimation.fromValue = pointLeft
        shakeAnimation.toValue = pointRight
        shakeAnimation.autoreverses = true
        shakeAnimation.repeatCount = (repeatCount > 0 ? repeatCount : 1)
        shakeAnimation.duration = 0.1
        self.layer.add(shakeAnimation, forKey: "springAnimateID")
    }
    
    
}
