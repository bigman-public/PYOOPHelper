//
//  ExtensionHelper.swift
//  Pods
//
//  Created by yunhe.lin on 17/3/1.
//
//

import Foundation

protocol StackContainer{
    
    associatedtype itemType //声明一个关联类型
    var count: Int { get }
    subscript(i: Int) -> itemType? { get }
    
}

public struct ElementStack <Element>: StackContainer {
    var items = [Element]()
    
    public init () {}
    
    public mutating func push(_ element: Element) {
        items.append(element)
    }
    
    @discardableResult
    public mutating func pop() -> Element? {
        guard items.count > 0 else {
            return nil
        }
        return items.removeLast()
    }
    
    typealias itemType = Element
    
    public var count: Int {
        return items.count
    }
    
    public subscript(i: Int) -> Element? {
        guard i < items.count else {
            return nil
        }
        return items[i]
    }
}

public func SwapValues <T>(_ a: inout T, _ b: inout T) -> Void {
    let  tempValue = a
    a = b
    b = tempValue
}
