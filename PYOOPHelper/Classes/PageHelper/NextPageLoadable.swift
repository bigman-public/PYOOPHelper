//
//  NextPageLoadable.swift
//  Pods
//
//  Created by yunhe.lin on 17/2/15.
//
//

import Foundation

public protocol ReloadableRype {
    
    func reloadData()
}

extension UIViewController: ReloadableRype {
    
    /// 提供reloadData 默认方法 需要继承
    open func reloadData() { }
}

public protocol NextPageLoadable: class {
    
    associatedtype DateType
    associatedtype LastIdType
    
    var datas: [DateType] { get set }
    var nextPageState: NextPageState<LastIdType> { get set }
    
    func performLoad(
        successHandler: (_ rows: [DateType], _ hasNext: Bool, _ lastId: LastIdType?) -> (),
        failHandler: () -> ())
    
}

extension NextPageLoadable where Self: UIViewController {
    
    public func loadNext() {
        guard nextPageState.hasNext else { return }
        guard !nextPageState.isLoading else { return }
        
        nextPageState.isLoading = true
        
        performLoad(successHandler: { (rows, hasNext, lastId) in
            self.datas = self.datas + rows
            self.nextPageState.update(hasNext: hasNext, isLoading: false, lastId: lastId)
            self.reloadData()
            print(self.datas)
        }) { 
            print("加载失败了")
            self.nextPageState.isLoading = false
        }
        
    }
}
