//
//  URLHandleable.swift
//  Pods
//
//  Created by yunhe.lin on 17/2/15.
//
//

import Foundation

public protocol URLHandlerable {
    
    @discardableResult
    func open(_ url: URL) -> Bool

}
