//
//  ViewController.swift
//  PYOOPHelper
//
//  Created by yunhe.lin on 02/10/2017.
//  Copyright (c) 2017 yunhe.lin. All rights reserved.
//

import UIKit
import PYOOPHelper

class ViewController: UIViewController {
    

    var nextPageState = NextPageState<Int>()
    var datas: [String] = []
    
    var tableView = UITableView.init()
    
    var button: PYButton = {
        let button = PYButton.init(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
        button.backgroundColor = UIColor.red
        return button
    }()
    
    var imageView: PYImageView = {
        let imageView = PYImageView.init(frame: CGRect.init(x: 100, y: 0, width: 100, height: 100))
        imageView.backgroundColor = UIColor.red
        return imageView;
    }()
    
    var label: PYLabel = {
        
        let label = PYLabel.init(frame: CGRect.init(x: 100, y: 100, width: 100, height: 100))
        return label
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        button.addTarget(self, action: #selector(pop(clickButton:)), for: .touchUpInside)
        view.addSubview(button)
        view.addSubview(imageView)
        view.addSubview(label)
        label.py_font(UIFont.systemFont(ofSize: 13)).py_text("dakdkajsdhka").py_textColor(.red).py_numberOfLines(2)
        // Do any additional setup after loading the view, typically from a nib.
        TargetHelper.instance.register(target: "app://sds", TestViewController.classForCoder())
        JumpHelper.instance.pushViewController("app://sds")
        
        var a = 10
        var b = 11
        print("a ---> \(a)\n b -------> \(b)")
        SwapValues(&a, &b)
        print("a ---> \(a)\n b -------> \(b)")
        
        var stack = ElementStack<Int>()
        stack.push(1)
        stack.push(2)
        let result = stack[1]
        print(result ?? 0)
        print(stack.pop(0) ?? 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - -handler
    func pop(clickButton: PYButton) -> Void {
//        button.py_pop(animated: true)
//        button.py_shake(repeatCount: 5)
//        button.py_spring(repeatCount: 5)
//        imageView.py_failLoading()
        self.loadNext()
        URLHandler.open(targetUrl: "test/hdh/dadsa")
        URLHandler.open(targetUrl: "test/hdh/dadsa")
        URLHandler.open(targetUrl: "test/hdh/dadsa")
    }
    
    
}

extension ViewController: NextPageLoadable {
    
    func performLoad(successHandler: ([String], Bool, Int?) -> (), failHandler: () -> ()) {
        if arc4random() % 2 == 0 {
            successHandler(["1"], true, 1)
        } else {
            failHandler()
        }
    }
    
    override func reloadData() {
        super.reloadData()
        print("我reload了")
    }

}



