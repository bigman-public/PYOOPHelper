#面向协议
####困境之一:
	(cross-cutting concerns 横切关注点)
	特性的组合要比继承更贴切事物的本质，无法描述两个不同事物具有某个相同特性这一点
	协议的extension
	protocol p {
		func method()
	}
	
	extension p {
	
		func method() {
			// 提供默认的实现方法
		}
		
		func anotherMethod() {
			// 协议外额外的方法
		}
	}
	
	extension p where Self: xxClass {
		// 对应到某种类的协议
	}
	
	~协议的定义
		·提供实现类的入口
		·遵循协议的类型需要对其进行实现
	~协议扩展
		·为入口提供默认实现
		·提供入口额外的实现
####	困境之二:
	(菱形缺陷)
	1.多继承
	2.base

		A
	|	   |
	B	   C
		|
		D
	继承关系
	
	protocol A {
		var name: String { get }
	}
	
	protocol B {
		var name: String { get }
	}
	
	// 这样并不会编译错误 同时满足 A，B的name	
	struct Person: A, B {
		var name: String?
	}
	
	// 不会出现编译错误 B中那么 也将使用 A种的name
	extension A {
		var name: String { 
			return "default name"
		}
	}
	
	// 多添加一个实现时 会出现编译错误 属性冲突
	extension B {
		var name: String { 
			return "default name B"
		}
	}
	
	这样的菱形缺陷确实还是没有很好的解决，同时包含了签名一样的属性或者方法时，
	你要重新进行实现，至少确定唯一性。
	ps：这样的菱形冲突在对于程序员来说还是能很好的规避。单单一个name已经无
	法满足可读性代码的规范。xx_name yy_name只有这样才能帮助我们很好的阅读代码。

	
#### 困境之三
	动态分发的安全性
	swift 安全校验性校验 代码层面就将其检查，编译器错误，不存在运行时出错
	-- does not conform to protocol 'xxx' protocol requires function 'xxxmethod'
	
	
#### Tips
优先考虑使用协议，高度协议化有助于解耦，测试以及扩展
结合泛型发挥协议特性，避免动态调用和类型转换，保证安全性
	
	
########总结
oop 三大困境，编辑器 语言的校验 都帮助我们从中逃脱出来。不过语言是死的，所以
菱形缺陷需要我们自己进行逃脱。
为什么写：个人认为 swift 一门严谨 但是 简洁的语言。严谨是 swift 在代码层面就进行了严格的校验，有那么一种你要用就要有我的规范。
个人觉得swift 对于新手来说是一门很好的语言，自身学习oc 来说 代码上面 起初"乱七八糟"。简洁，减少了冗余代码。让一行代码读完你就知道这一行代码是干什么的了。