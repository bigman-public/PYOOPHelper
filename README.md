
# PYOOPHelper

Swift -- OOP 有什么理由不学习 

## Example

#####1.URLHandler 打开url 帮助插件

#####2.PageHelper 分页插件 

## Requirements

[oop 小笔记](https://gitlab.com/bigman-public/PYOOPHelper/blob/master/oop.md) 小笔记

## Installation

To install it, simply add the following line to your Podfile:

```ruby
pod "PYOOPHelper", git => "https://gitlab.com/bigman-public/PYOOPHelper.git"
```

## Author

yunhe.lin, bigmanhxx@gamil.com

## @other

[PYLog] (https://gitlab.com/bigman-public/PYLog.git ) 重载运算符 log tool


